# NeuNet
Exploration into creating a neuromorphic neural network of McCulloch-Pitts Neurons

## Register
A `Register` is the input / output of the network. Think of this as register space in a typical CPU. They are basic bit arrays that either input a signal to the network or receive an output signal from the network.

## Neuron
The heart of the network is the McCulloch-Pitts Neuron (MCP).  A typical MCP follows a summation function, `y = m * x`, followed by a threshold function, `y' = {1 if y >= T, else 0}`. The weights, `m`, for each input bit, `x`, as well as the threshold, `T` are known at creation. In fact, they are made to force the expected output. There is no training or flexibility in a neuron.

For our MCP we will apply the weight to the output instead of the input, where the weight is the wilson score interval for the neighboring neuron. Since each input is handled independently as a message as it is received instead of all at once the input, `x`, needs to be summed with current level, `C`, and a time decay, `td`, needs to be applied. This leaves us with the function, `y = (x + C) * e ^ (-td * t)`, where:
 - `t` is time since last message
 - `td` is average frequency of messages received divided by 2

We use the frequency of messaging divided by two to ensure that more active neurons still apply a decay between messaging while less active neurons have a longer decay.

The thresholding function remains unchanged and the threshold, `T`, is assigned a random value at initialization.

## Network
The network starts out with one neuron on each bit in the register. When a neuron is activated it will forward the message to all known neighbors. If less than ten neighbors are known a message is sent to a random neuron. If that neuron does not exist it will be created. To train a network a positive feedback is provided that backpropogates through the path. Positive neighbors are given an "up vote", others are given a "down vote". Neighbors that fall below a certain threshold are "forgotten" and a new neighbor takes its place. This weighting enforces a plasticity in learned paths while still allowing flexibility to learn new paths.  Much like the flight patterns of bees finding pollen. The message passed 
